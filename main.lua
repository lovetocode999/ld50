--[[

Copyright (c) 2022 Elijah Gregg

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

--]]

-- Load Sysl-Pixel
gscreen = require("lib/sysl/pixel")
gscreen.current_cursor = 2
gscreen.load(2)
-- Load HUMP libraries
gamestate = require("lib/hump/gamestate")
class = require("lib/hump/class")
-- Bump for collision detection because I'm too lazy to do it myself
bump = require("lib/bump/bump")

-- Create main gamestate
local game = {}

-- Create gameover gamestate
local gameover = {}

-- Create menu gamestate
local menu = {}

-- Create tutorial gamestate
local tutorial = {}

-- Init the tutorial gamestate
function tutorial:init()
  gscreen.current_cursor = 1
  self.background = love.graphics.newImage("assets/tutorial.png")
  self.font = love.graphics.newImageFont("assets/tutorial-font.png", "0123456789abcdefghijklmnopqrstuvwxyz.,! ", 2)
  -- UI elements
  self.uielements = {
     uielementList = {}
  }
  -- New UI
  function self.uielements:add(ui)
    table.insert(self.uielementList, ui)
  end
  -- Delete a UI
  function self.uielements:remove(ui)
    for i, e in ipairs(self.uielementList) do
      if e == ui then
        table.remove(self.uielementList, i)
        return
      end
    end
  end
  -- Draw all the UI elements
  function self.uielements:draw()
    for i, e in ipairs(self.uielementList) do
      e:draw()
    end
  end
  -- Update all the UI elements
  function self.uielements:update(dt)
    for i, e in ipairs(self.uielementList) do
      e:update(dt)
    end
  end
  -- UI element
  self.ui = class{}
  -- Init UI element
  function self.ui:init()
  end
  -- Empty base functions for UI elements
  function self.ui:draw()
  end
  function self.ui:update(dt)
  end
  -- Main tutorial UI
  self.main = class{
    _includes = self.ui
  }
  function self.main:init()
    self.counter = 0
  end
  function self.main:update(dt)
    self.counter = self.counter + dt
    if love.mouse.isDown(1) and self.counter > 0.4 then
      gamestate.switch(game)
    end
  end
  function self.main:draw()
    love.graphics.setColor(1, 1, 1)
    love.graphics.setFont(tutorial.font)
    love.graphics.printf("hold off the sky pirate frogs for as long as possible to try and save your island. click anywhere on the island to open the shop, and click anywhere in the sky to shoot down the frogs. good luck!", 10, 107, 160, "left")
    love.graphics.printf("click anywhere to continue...", 0, 197, 180, "center")
  end
end

-- Enter the tutorial
function tutorial:enter()
  self.uielements:add(self.main())
end

-- Update the tutorial
function tutorial:update(dt)
  -- Update Sysl-Pixel
  gscreen.update(dt)
  -- Update the UI
  self.uielements:update(dt)
end

-- Draw the tutorial
function tutorial:draw()
  -- Scale stuff with Sysl-Pixel
  gscreen.start()
  love.graphics.setColor(1, 1, 1)
  love.graphics.draw(self.background)
  self.uielements:draw()
  -- Stop scaling stuff
  gscreen.stop()
end

-- Init the menu gamestate
function menu:init()
  gscreen.current_cursor = 1
  self.background = love.graphics.newImage("assets/menu-background.png")
  self.playimage = {}
  self.playimage[1] = love.graphics.newImage("assets/playbutton.png")
  self.playimage[2] = love.graphics.newImage("assets/playbutton-2.png")
  -- UI elements
  self.uielements = {
     uielementList = {}
  }
  -- New UI
  function self.uielements:add(ui)
    table.insert(self.uielementList, ui)
  end
  -- Delete a UI
  function self.uielements:remove(ui)
    for i, e in ipairs(self.uielementList) do
      if e == ui then
        table.remove(self.uielementList, i)
        return
      end
    end
  end
  -- Draw all the UI elements
  function self.uielements:draw()
    for i, e in ipairs(self.uielementList) do
      e:draw()
    end
  end
  -- Update all the UI elements
  function self.uielements:update(dt)
    for i, e in ipairs(self.uielementList) do
      e:update(dt)
    end
  end
  -- UI element
  self.ui = class{}
  -- Init UI element
  function self.ui:init()
  end
  -- Empty base functions for UI elements
  function self.ui:draw()
  end
  function self.ui:update(dt)
  end
  -- Main menu UI
  self.main = class{
    _includes = self.ui
  }
  function self.main:init()
  end
  function self.main:update(dt)
    if gscreen.mouse.y > 142 and gscreen.mouse.y < 178 and gscreen.mouse.x > 55 and gscreen.mouse.x < 125 and love.mouse.isDown(1) then
      gamestate.switch(tutorial)
    end
  end
  function self.main:draw()
    love.graphics.setColor(1, 1, 1)
    if gscreen.mouse.y > 142 and gscreen.mouse.y < 178 and gscreen.mouse.x > 55 and gscreen.mouse.x < 125 then
      love.graphics.draw(menu.playimage[2], 55, 142)
    else
      love.graphics.draw(menu.playimage[1], 55, 142)
    end
  end
end

-- Enter the menu
function menu:enter()
  self.uielements:add(self.main())
end

-- Update the menu
function menu:update(dt)
  -- Update Sysl-Pixel
  gscreen.update(dt)
  -- Update the UI
  self.uielements:update(dt)
end

-- Draw the menu
function menu:draw()
  -- Scale stuff with Sysl-Pixel
  gscreen.start()
  love.graphics.setColor(1, 1, 1)
  love.graphics.draw(self.background)
  self.uielements:draw()
  -- Stop scaling stuff
  gscreen.stop()
end

-- Init the gameover gamestate
function gameover:init()
  self.gameoverimage = love.graphics.newImage("assets/gameover-text.png")
  self.font = love.graphics.newImageFont("assets/gameover-font.png", "yousrviedwacn0123456789., ", 2)
  gscreen.current_cursor = 1
  -- Create variables for tilemap
  self.tile = {}
  for i = 0, 9 do
    self.tile[i] = love.graphics.newImage("assets/tile"..i..".png")
  end
  -- Map variables
  self.map_w = 18
  self.map_h = 32
  self.map_x = 0
  self.map_y = 0
  self.map_offset_x = 0
  self.map_offset_y = 0
  self.map_display_w = 18
  self.map_display_h = 32
  self.tile_w = 10
  self.tile_h = 10
  self.map = {
     {1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3}, 
     {0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4},
     {6, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 6},
     {6, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 6},
     {7, 6, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 6, 8},
     {9, 6, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 6, 9},
     {9, 7, 6, 0, 5, 2, 2, 2, 2, 2, 2, 2, 2, 5, 4, 6, 8, 9},
     {9, 9, 7, 6, 6, 0, 5, 2, 2, 2, 2, 5, 4, 6, 6, 8, 9, 9},
     {9, 9, 9, 9, 7, 6, 6, 0, 5, 5, 4, 6, 6, 8, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 7, 6, 6, 6, 6, 8, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
  }
  -- UI elements
  self.uielements = {
     uielementList = {}
  }
  -- New UI
  function self.uielements:add(ui)
    table.insert(self.uielementList, ui)
  end
  -- Delete a UI
  function self.uielements:remove(ui)
    for i, e in ipairs(self.uielementList) do
      if e == ui then
        table.remove(self.uielementList, i)
        return
      end
    end
  end
  -- Draw all the UI elements
  function self.uielements:draw()
    for i, e in ipairs(self.uielementList) do
      e:draw()
    end
  end
  -- Update all the UI elements
  function self.uielements:update(dt)
    for i, e in ipairs(self.uielementList) do
      e:update(dt)
    end
  end
  -- UI element
  self.ui = class{}
  -- Init UI element
  function self.ui:init()
  end
  -- Empty base functions for UI elements
  function self.ui:draw()
  end
  function self.ui:update(dt)
  end
  -- Main gameover UI
  self.main = class{
    _includes = self.ui
  }
  function self.main:init()
  end
  function self.main:update(dt)
  end
  function self.main:draw()
    love.graphics.setColor(1, 1, 1)
    love.graphics.draw(gameover.gameoverimage, 16, 135)
    love.graphics.setFont(gameover.font)
    love.graphics.printf("you survived "..gameover.wave - 1 .." waves, "..math.floor(gameover.time_playing * 3000) / 100 .."  seconds",
                         0, 160, 180, "center")
  end
end

-- Enter the gameover gamestate
function gameover:enter(old_state, wave, time_playing)
  self.wave = wave
  self.time_playing = time_playing
  self.uielements:add(self.main())
end

-- Draw the gameover gamestate
function gameover:draw()
  -- Scale stuff with Sysl-Pixel
  gscreen.start()
  -- Background
  love.graphics.setColor(44/255, 232/255, 244/255)
  love.graphics.rectangle("fill", 0, 0, 180, 320)
  love.graphics.setColor(1, 1, 1)
  self:draw_map()
  -- Draw the UI
  self.uielements:draw()
  -- Stop scaling stuff
  gscreen.stop()
end

-- Update the gameover gamestate
function gameover:update(dt)
  -- Update Sysl-Pixel
  gscreen.update(dt)
end

-- Init the "game" gamestate
function game:init()
  -- Set a new random seed
  math.randomseed(os.time())
  -- Create a new world for bump to use
  self.world = bump.newWorld(10)
  -- Create variables for tilemap
  self.tile = {}
  for i = 0, 9 do
    self.tile[i] = love.graphics.newImage("assets/tile"..i..".png")
  end
  -- Set the cursor
  gscreen.current_cursor = 2
  -- Miscellaneous variables
  self.counter = 0
  self.movement_que = nil
  self.time_playing = 0
  self.wave = 0
  self.last_wave = 0
  self.new_wave = true
  self.placing = false
  self.placing_icon = love.graphics.newImage("assets/towerselect.png")
  self.font = love.graphics.newImageFont("assets/font.png", "0123456789$", 2)
  self.smallfont = love.graphics.newImageFont("assets/smallfont.png", "0123456789$", 1)
  self.largefont = love.graphics.newImageFont("assets/largefont.png", "0123456789", 4)
  self.money = 0
  self.lives = 4
  -- Music and SFX
  self.explosion_sound = love.audio.newSource(love.sound.newSoundData("assets/explosion.wav"), "static")
  self.music = love.audio.newSource("assets/music.wav", "stream")
  self.music:setLooping(true)
  self.music:play()
  -- Map variables
  self.map_w = 18
  self.map_h = 32
  self.map_x = 0
  self.map_y = 0
  self.map_offset_x = 0
  self.map_offset_y = 0
  self.map_display_w = 18
  self.map_display_h = 32
  self.tile_w = 10
  self.tile_h = 10
  self.map = {
     {1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3}, 
     {0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4},
     {6, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 6},
     {6, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 6},
     {7, 6, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 6, 8},
     {9, 6, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 6, 9},
     {9, 7, 6, 0, 5, 2, 2, 2, 2, 2, 2, 2, 2, 5, 4, 6, 8, 9},
     {9, 9, 7, 6, 6, 0, 5, 2, 2, 2, 2, 5, 4, 6, 6, 8, 9, 9},
     {9, 9, 9, 9, 7, 6, 6, 0, 5, 5, 4, 6, 6, 8, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 7, 6, 6, 6, 6, 8, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
     {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
  }
  -- Entities variable
  self.entities = {
    entityList = {}
  }
  -- Create a new entity
  function self.entities:add(entity)
    table.insert(self.entityList, entity)
  end
  -- Delete an entity
  function self.entities:remove(entity)
    for i, e in ipairs(self.entityList) do
      if e == entity then
        game.world:remove(entity)
        table.remove(self.entityList, i)
        return
      end
    end
  end
  -- Draw all entities
  function self.entities:draw()
    for i, e in ipairs(self.entityList) do
      e:draw()
    end
  end
  -- Update all entities
  function self.entities:update(dt)
    for i, e in ipairs(self.entityList) do
      e:update(dt)
    end
  end
  -- Enemies variable
  self.enemies = {
    enemyList = {}
  }
  -- Create a new enemy
  function self.enemies:add(enemy)
    table.insert(self.enemyList, enemy)
  end
  -- Delete an enemy
  function self.enemies:remove(enemy)
    for i, e in ipairs(self.enemyList) do
      if e == enemy then
        game.world:remove(enemy)
        table.remove(self.enemyList, i)
        return
      end
    end
  end
  -- Draw all enemies
  function self.enemies:draw()
    for i, e in ipairs(self.enemyList) do
      e:draw()
    end
  end
  -- Update all enemies
  function self.enemies:update(dt)
    for i, e in ipairs(self.enemyList) do
      e:update(dt)
    end
  end
  -- Base entity object
  self.entity = class{}
  -- Init entity
  function self.entity:init(x, y, w, h)
    self.x = x
    self.y = y
    self.isBullet = false
    self.isEnemy = false
    self.isTower = false
    game.world:add(self, x * game.tile_w, y * game.tile_h, w, h)
  end
  -- Empty base functions for entities
  function self.entity:draw()
  end
  function self.entity:update(dt)
  end
  -- Enemy class
  self.enemy = class{
    _includes = self.entity
  }
  for i = 1, 2 do
    self.enemy["image"..i] = love.graphics.newImage("assets/enemy"..i..".png")
  end
  -- Enemy init function
  function self.enemy:init(entity, x, y, enemyType)
    if enemyType == 1 then
      entity.init(self, x, y, 10, 20)
    elseif enemyType == 2 then
      entity.init(self, x, y, 20, 40)
    end
    for i = 0, 4 do
      self["image"..i * 45] = love.graphics.newImage("assets/enemy"..enemyType.."-"..45*i..".png")
    end
    self.counter = 0
    self.bulletCounter = 0
    self.enemyType = enemyType
    self.hp = enemyType == 1 and 2 + game.wave - 1 or 5 + 2 * game.wave - 2
    self.max_hp = self.hp
    self.isBullet = false
    self.isTower = false
    self.isEnemy = true
    self.targetTower = game.towers.towerList[math.random(#game.towers.towerList)]
  end
  -- Enemy update function
  function self.enemy:update(dt)
    self.counter = self.counter + dt
    self.bulletCounter = self.bulletCounter + dt
    if self.counter > 0.05 / math.ceil(game.time_playing / 2) then
      self.counter = 0
      goalx = self.x * game.tile_w
      goaly = ((self.y - game.map_y - 0.1) * game.tile_h)
      local actualX, actualY, cols, len = game.world:move(self, goalx, goaly, self.collisionFilter)
      self.y = (actualY / game.tile_h) + game.map_y
    end
    if self.bulletCounter > 1 and self.y < 16 and self.targetTower ~= nil then
      self.bulletCounter = 0
      local direction = math.atan((self.targetTower.y - self.y) /
                                  (self.x - self.targetTower.x))
      if self.targetTower.x < self.x then
         direction = direction + math.pi
      end
      if self.targetTower.x == self.x then
         direction = -1 * direction
      end
      game.entities:add(game.cannonball(game.entity, self.x, self.y, direction))
    end
    if self.y < -1 * self.enemyType then
      game.enemies:remove(self)
      game.lives = game.lives - self.enemyType
      if game.lives <= 0 then
        love.audio.stop()
        gamestate.switch(gameover, game.wave, game.time_playing)
      end
      return
    end
  end
  -- Enemy draw function
  function self.enemy:draw()
    love.graphics.setColor(1, 1, 1)
    local direction = 90
    if self.targetTower ~= nil then
      direction = math.floor(((math.atan((self.targetTower.y - self.y) /
                             (self.x - self.targetTower.x))) * 8) / math.pi) * 22.5
      if self.targetTower.x < self.x then
         direction = direction + 180
      end
      if self.targetTower.x == self.x then
         direction = -1 * direction
      end
      if math.floor(direction / 45) ~= direction / 45 then
        direction = math.ceil(direction / 45) * 45
      end
      if direction < 0 then
        direction = 360 + direction
      end
      if direction > 360 then
        direction = direction - 360
      end
      if direction == -0 then
        direction = 0
      end
      if direction > 180 then
        if direction < 270 then
          direction = 180
        else
          direction = 0
        end
      end
    end
    love.graphics.draw(self["image"..direction], self.x * game.tile_w, (self.y - game.map_y) * game.tile_h)
    local percentage = self.hp / self.max_hp
    love.graphics.setColor(63 / 255, 40 / 255, 50 / 255)
    love.graphics.setLineWidth(3)
    love.graphics.line(self.x * game.tile_w, ((self.y - game.map_y) * game.tile_h) - 4,
                       (self.x * game.tile_w) + (self.enemyType * 10), ((self.y - game.map_y) * game.tile_h) - 4)
    love.graphics.setLineWidth(1)
    love.graphics.line(self.x * game.tile_w - 1, ((self.y - game.map_y) * game.tile_h) - 4,
                       (self.x * game.tile_w) + (self.enemyType * 10) + 1, ((self.y - game.map_y) * game.tile_h) - 4)
    if percentage > 5 / 6 then
      love.graphics.setColor(50 / 255, 115 / 255, 69 / 255)
    elseif percentage > 4 / 6 then
      love.graphics.setColor(99 / 255, 198 / 255, 77 / 255)
    elseif percentage > 0.5 then
      love.graphics.setColor(1, 231 / 255, 98 / 255)
    elseif percentage > 2 / 6 then
      love.graphics.setColor(251 / 255, 146 / 255, 43 / 255)
    elseif percentage > 1 / 6 then
      love.graphics.setColor(229 / 255, 59 / 255, 68 / 255)
    else
      love.graphics.setColor(158 / 255, 40 / 255, 53 / 255)
    end
    love.graphics.line(self.x * game.tile_w, ((self.y - game.map_y) * game.tile_h) - 4,
                       (self.x * game.tile_w) + (self.enemyType * 10 * (self.hp / self.max_hp)), ((self.y - game.map_y) * game.tile_h) - 4)
    love.graphics.setColor(1, 1, 1)
  end
  -- Enemy collision filter
  function self.enemy.collisionFilter(item, other)
     return nil
  end
  -- UI elements
  self.uielements = {
     uielementList = {}
  }
  -- New UI
  function self.uielements:add(ui)
    table.insert(self.uielementList, ui)
  end
  -- Delete a UI
  function self.uielements:remove(ui)
    for i, e in ipairs(self.uielementList) do
      if e == ui then
        table.remove(self.uielementList, i)
        return
      end
    end
  end
  -- Draw all the UI elements
  function self.uielements:draw()
    for i, e in ipairs(self.uielementList) do
      e:draw()
    end
  end
  -- Update all the UI elements
  function self.uielements:update(dt)
    for i, e in ipairs(self.uielementList) do
      e:update(dt)
    end
  end
  -- UI element
  self.ui = class{}
  -- Init UI element
  function self.ui:init()
    self.isShop = false
  end
  -- Empty base functions for UI elements
  function self.ui:draw()
  end
  function self.ui:update(dt)
  end
  -- Info UI
  self.info = class{
    _includes = self.ui
  }
  -- Init the info UI (lives, money)
  function self.info:init()
    self.live_img = love.graphics.newImage("assets/fly.png")
    self.wave_text = love.graphics.newImage("assets/wave-text.png")
    self.counter = 0
  end
  -- Empty update function (this UI can not be interacted with)
  function self.info:update(dt)
    self.counter = self.counter + dt
    if game.new_wave == true then
      self.counter = 0
    end
  end
  -- Draw the info UI
  function self.info:draw()
    love.graphics.setColor(1, 1, 1)
    love.graphics.setFont(game.font)
    love.graphics.print(game.money.."$", 5, 3)
    for i = 1, game.lives do
      love.graphics.draw(self.live_img, 180 - (20 * i), 0)
    end
    if self.counter < 5 then
      love.graphics.draw(self.wave_text, 44, 150)
      love.graphics.setFont(game.largefont)
      love.graphics.print(game.wave, 124, 150)
    end
  end
  self.uielements:add(self.info())
  -- Shop UI
  self.shop = class{
    _includes = self.ui
  }
  -- Init the shop for the tower at x, y
  function self.shop:init(x, y)
    game.hasShopmenu = true
    self.counter = 0
    self.x = x / game.map_w
    self.y = y / game.map_h
    self.isShop = true
    local isExisting = false
    for i, e in ipairs(game.towers.towerList) do
      if e.x == x and e.y == y then
        isExisting = true
        self.targetTower = e
        break
      end
    end
    if isExisting then
      self.type = 1
      self.powerimage, self.speedimage, self.sellimage = {}, {}, {}
      self.background = love.graphics.newImage("assets/shopmenu-background.png")
      for i = 1, 4 do
        self.powerimage[i] = love.graphics.newImage("assets/shopmenu-upgrade-power"..i..".png")
        self.speedimage[i] = love.graphics.newImage("assets/shopmenu-upgrade-speed"..i..".png")
      end
      self.sellimage[1] = love.graphics.newImage("assets/shopmenu-upgrade-sell"..1 ..".png")
      self.sellimage[2] = love.graphics.newImage("assets/shopmenu-upgrade-sell"..2 ..".png")
    else
      self.type = 2
      self.cannonimage, self.trebuchetimage = {}, {}
      self.background = love.graphics.newImage("assets/shopmenu-background.png")
      for i = 1, 2 do
        self.trebuchetimage[i] = love.graphics.newImage("assets/shopmenu-buy-trebuchet"..i..".png")
        self.cannonimage[i] = love.graphics.newImage("assets/shopmenu-buy-cannon"..i..".png")
      end
    end
  end
  -- Update the shop menu
  function self.shop:update(dt)
    self.counter = self.counter + dt
    if self.type == 1 then
      if love.mouse.isDown(1) and self.counter > 0.4 then
        if gscreen.mouse.y > 252 and gscreen.mouse.y < 260 then
          game.uielements:remove(self)
          game.hasShopmenu = false
          return
        elseif gscreen.mouse.y > 260 then
          if gscreen.mouse.x < 60 then 
            if game.money >= self.targetTower.power * 10 then
              game.money = game.money - self.targetTower.power * 10
              self.targetTower.power = self.targetTower.power + 1
              self.counter = 0
            end
          elseif gscreen.mouse.x > 60 and gscreen.mouse.x < 120 then
            if game.money >= self.targetTower.shooting_speed * 10 then
              game.money = game.money - self.targetTower.shooting_speed * 10
              self.targetTower.shooting_speed = self.targetTower.shooting_speed + 1
              self.counter = 0
            end
          elseif gscreen.mouse.x > 120 then
            game.money = game.money + self.targetTower.cost
            game.towers:remove(self.targetTower)
            game.uielements:remove(self)
            game.hasShopmenu = false
            return
          end
        end
      end
    else
      if love.mouse.isDown(1) and self.counter > 0.4 then
        if gscreen.mouse.y > 252 and gscreen.mouse.y < 260 then
          game.uielements:remove(self)
          game.hasShopmenu = false
          return
        elseif gscreen.mouse.y > 260 then
          if gscreen.mouse.x < 90 then
            if game.money >= 100 then
              game.money = game.money - 100
              game.towers:add(game.tower(game.entity, self.x * game.map_w, self.y * game.map_h, 1))
              game.uielements:remove(self)
              game.hasShopmenu = false
              self.counter = 0
              return
            end
          elseif gscreen.mouse.x > 90 then
            if game.money >= 200 then
              game.money = game.money - 200
              game.towers:add(game.tower(game.entity, self.x * game.map_w, self.y * game.map_h, 2))
              game.uielements:remove(self)
              game.hasShopmenu = false
              self.counter = 0
              return
            end
          end
        end
      end
    end
  end
  -- Draw the shop menu
  function self.shop:draw()
    if self.type == 1 then
      local powerchange = 0
      local speedchange = 0
      if self.targetTower.power >= 5 then
        powerchange = 2
      end
      if self.targetTower.shooting_speed >= 5 then
        speedchange = 2
      end
      if gscreen.mouse.y > 260 then
        love.graphics.draw(self.background, 0, 253)
        if gscreen.mouse.x < 60 then
          love.graphics.draw(self.powerimage[2 + powerchange], 0, 260)
          love.graphics.draw(self.speedimage[1 + speedchange], 60, 260)
          love.graphics.draw(self.sellimage[1], 120, 260)
        elseif gscreen.mouse.x > 60 and gscreen.mouse.x < 120 then
          love.graphics.draw(self.powerimage[1 + powerchange], 0, 260)
          love.graphics.draw(self.speedimage[2 + speedchange], 60, 260)
          love.graphics.draw(self.sellimage[1], 120, 260)
        elseif gscreen.mouse.x > 120 then
          love.graphics.draw(self.powerimage[1 + powerchange], 0, 260)
          love.graphics.draw(self.speedimage[1 + speedchange], 60, 260)
          love.graphics.draw(self.sellimage[2], 120, 260)
        else
          love.graphics.draw(self.powerimage[1 + powerchange], 0, 260)
          love.graphics.draw(self.speedimage[1 + speedchange], 60, 260)
          love.graphics.draw(self.sellimage[1], 120, 260)
        end
      elseif gscreen.mouse.y > 252 then
        love.graphics.draw(self.background, 0, 253)
        love.graphics.draw(self.powerimage[1 + powerchange], 0, 260)
        love.graphics.draw(self.speedimage[1 + speedchange], 60, 260)
        love.graphics.draw(self.sellimage[1], 120, 260)
      else
        love.graphics.draw(self.background, 0, 253)
        love.graphics.draw(self.powerimage[1 + powerchange], 0, 260)
        love.graphics.draw(self.speedimage[1 + speedchange], 60, 260)
        love.graphics.draw(self.sellimage[1], 120, 260)
      end
      love.graphics.setFont(game.smallfont)
      if self.targetTower.power < 5 then
        love.graphics.print((self.targetTower.power * 10).."$", 36, 283)
        love.graphics.print(self.targetTower.power, 42, 297)
      end
      if self.targetTower.shooting_speed < 5 then
        love.graphics.print((self.targetTower.shooting_speed * 10).."$", 96, 283)
        love.graphics.print(self.targetTower.shooting_speed, 102, 297)
      end
      love.graphics.print((self.targetTower.cost).."$", 156, 283)
    elseif self.type == 2 then
      love.graphics.draw(self.background, 0, 253)
      if gscreen.mouse.y > 260 then
        if gscreen.mouse.x < 90 then
          love.graphics.draw(self.trebuchetimage[2], 0, 260)
          love.graphics.draw(self.cannonimage[1], 90, 260)
        elseif gscreen.mouse.x > 90 then
          love.graphics.draw(self.trebuchetimage[1], 0, 260)
          love.graphics.draw(self.cannonimage[2], 90, 260)
        else
          love.graphics.draw(self.trebuchetimage[1], 0, 260)
          love.graphics.draw(self.cannonimage[1], 90, 260)
        end
      else
        love.graphics.draw(self.trebuchetimage[1], 0, 260)
        love.graphics.draw(self.cannonimage[1], 90, 260)
      end
      love.graphics.setFont(game.smallfont)
      love.graphics.print(100 .."$", 51, 283)
      love.graphics.print(200 .."$", 141, 283)
    end
  end
  -- Towers
  self.towers = {
    towerList = {}
  }
  -- Create a new tower
  function self.towers:add(tower)
    table.insert(self.towerList, tower)
  end
  -- Delete a tower
  function self.towers:remove(tower)
    for i, e in ipairs(self.towerList) do
      if e == tower then
        game.world:remove(tower)
        table.remove(self.towerList, i)
        return
      end
    end
  end
  -- Draw all towers
  function self.towers:draw()
    for i, e in ipairs(self.towerList) do
      e:draw()
    end
  end
  -- Update all towers
  function self.towers:update(dt)
    for i, e in ipairs(self.towerList) do
      e:update(dt)
    end
  end
  -- Tower class
  self.tower = class{
    _includes = self.entity
  }
  for i = 1, 2 do
    self.tower["image"..i] = love.graphics.newImage("assets/tower"..i..".png")
  end
  -- Tower init function
  function self.tower:init(entity, x, y, towerType)
    entity.init(self, x, y, 10, 10)
    self.cost = towerType * 15
    self.hp = towerType * 5
    self.max_hp = self.hp
    self.counter = 0
    self.towerType = towerType
    self.shooting_speed = 1
    self.power = 1
    self.isEnemy = false
    self.isBullet = false
    self.isTower = true
  end
  -- Tower update function
  function self.tower:update(dt)
    self.counter = self.counter + dt
    if self.counter > (0.2 / (self.towerType * math.sqrt(self.shooting_speed) * (1 / 3))) and love.mouse.isDown(1) and not game.placing then
      if not (game.hasShopmenu and gscreen.mouse.y > 252) then
        self.counter = 0
        local direction = math.atan((gscreen.mouse.y - ((self.y - game.map_y) * game.tile_h) + 5) /
                                    ((self.x * game.tile_w) + 5 - gscreen.mouse.x))
        if gscreen.mouse.x < (self.x * game.tile_w) + 5 then
           direction = direction + math.pi
        end
        if gscreen.mouse.x == (self.x * game.tile_w) + 5 then
           direction = -1 * direction
        end
        game.entities:add(game.bullet(game.entity, self.x, self.y, direction, self.towerType * self.shooting_speed, self.power * self.towerType))
      end
    end
  end
  -- Tower draw function
  function self.tower:draw()
    love.graphics.setColor(1, 1, 1)
    love.graphics.draw(self["image"..self.towerType], self.x * game.tile_w, (self.y - game.map_y) * game.tile_h)
    local percentage = self.hp / self.max_hp
    love.graphics.setColor(63 / 255, 40 / 255, 50 / 255)
    love.graphics.setLineWidth(3)
    love.graphics.line(self.x * game.tile_w, ((self.y - game.map_y) * game.tile_h) - 4,
                       (self.x * game.tile_w) + game.tile_w, ((self.y - game.map_y) * game.tile_h) - 4)
    love.graphics.setLineWidth(1)
    love.graphics.line(self.x * game.tile_w - 1, ((self.y - game.map_y) * game.tile_h) - 4,
                       (self.x * game.tile_w) + game.tile_w + 1, ((self.y - game.map_y) * game.tile_h) - 4)
    if percentage > 5 / 6 then
      love.graphics.setColor(50 / 255, 115 / 255, 69 / 255)
    elseif percentage > 4 / 6 then
      love.graphics.setColor(99 / 255, 198 / 255, 77 / 255)
    elseif percentage > 0.5 then
      love.graphics.setColor(1, 231 / 255, 98 / 255)
    elseif percentage > 2 / 6 then
      love.graphics.setColor(251 / 255, 146 / 255, 43 / 255)
    elseif percentage > 1 / 6 then
      love.graphics.setColor(229 / 255, 59 / 255, 68 / 255)
    else
      love.graphics.setColor(158 / 255, 40 / 255, 53 / 255)
    end
    love.graphics.line(self.x * game.tile_w, ((self.y - game.map_y) * game.tile_h) - 4,
                       (self.x * game.tile_w) + (game.tile_w * (self.hp / self.max_hp)), ((self.y - game.map_y) * game.tile_h) - 4)
    love.graphics.setColor(1, 1, 1)
  end
  -- Create the first tower
  local randx = math.random(2, 16)
  local randy = math.random(9)
  if self.map[randy+1][randx+1] > 5 then
    while self.map[randy+1][randx+1] > 5 do
      randy = randy - 1
    end
    if randy < 0 then randy = 0 end
  end
  self.towers:add(self.tower(self.entity, randx, randy, 1))
  -- Bullet class
  self.bullet = class{
     _includes = self.entity
  }
  -- Bullet init function
  function self.bullet:init(entity, x, y, direction, speed, power)
    entity.init(self, x, y, 7 * speed, 7 * speed)
    self.direction = direction
    self.power = power
    self.speed = math.sqrt(speed) / 3
    self.isBullet = true
    self["image"..180] = love.graphics.newImage("assets/bullet-180.png")
    for i = 0, 8 do
      self["image"..i*45] = love.graphics.newImage("assets/bullet-"..45*i..".png")
    end
  end
  -- Bullet update function
  function self.bullet:update(dt)
    if self.direction < 0 then
      self.direction = 2 * math.pi + self.direction
    end
    if self.direction > 2 * math.pi then
      self.direction = self.direction - 2 * math.pi
    end
    goaly = self.y + (0.25 * self.speed * math.sin(-1 * self.direction))
    goalx = self.x + (0.25 * self.speed * math.cos(-1 * self.direction))
    goalx = (goalx * game.tile_w) - 5
    goaly = ((goaly - game.map_y) * game.tile_h) - 5
    local actualX, actualY, cols, len = game.world:move(self, goalx, goaly, self.collisionFilter)
    self.x = (actualX + 5) / game.tile_w
    self.y = ((actualY + 5) / game.tile_h) + game.map_y
    for i = 1, len do
      local other = cols[i].other
      if other.isEnemy then
        game.play_sound(game.explosion_sound)
        other.hp = other.hp - self.power
        if other.hp <= 0 then
          game.money = game.money + 5 * other.enemyType
          game.enemies:remove(other)
        end
        game.entities:remove(self)
      elseif other.iscannonball then
        game.play_sound(game.explosion_sound)
        game.entities:remove(other)
        game.entities:remove(self)
      end
    end
    if self.x > 180 or self.x < -7 or self.y < -7 or self.y > 320 then
      game.entities:remove(self)
    end
  end
  -- Bullet draw function
  function self.bullet:draw()
    love.graphics.setColor(1, 1, 1)
    local direction = math.floor((self.direction * 8) / math.pi) * 22.5
    if math.floor(direction / 45) ~= direction / 45 then
      direction = math.ceil(direction / 45) * 45
    end
    if direction < 0 then
      direction = 360 + direction
    end
    if direction > 360 then
      direction = direction - 360
    end
    if direction == -0 then
      direction = 0
    end
    love.graphics.draw(self["image"..direction], (self.x * game.tile_w), ((self.y - game.map_y) * game.tile_h))
  end
  -- Bullet collision filter
  function self.bullet.collisionFilter(item, other)
     if other.isEnemy or other.iscannonball then return "cross"
     else return nil end
  end
  -- Cannonball class
  self.cannonball = class{
     _includes = self.entity
  }
  -- Cannonball init function
  function self.cannonball:init(entity, x, y, direction)
    entity.init(self, x, y, 7, 7)
    self.direction = direction
    self.iscannonball = true
    self.image = love.graphics.newImage("assets/cannonball.png")
  end
  -- Cannonball update function
  function self.cannonball:update(dt)
    goaly = self.y + (0.25 * math.sin(-1 * self.direction))
    goalx = self.x + (0.25 * math.cos(-1 * self.direction))
    goalx = (goalx * game.tile_w) + 5
    goaly = ((goaly - game.map_y) * game.tile_h) + 5
    local actualX, actualY, cols, len = game.world:move(self, goalx, goaly, self.collisionFilter)
    self.x = (actualX - 5) / game.tile_w
    self.y = ((actualY - 5) / game.tile_h) + game.map_y
    for i = 1, len do
      local other = cols[i].other
      if other.isTower then
        other.hp = other.hp - 1
        if other.hp <= 0 then
          game.towers:remove(other)
        end
        game.entities:remove(self)
      end
    end
  end
  -- Cannonball draw function
  function self.cannonball:draw()
    love.graphics.setColor(1, 1, 1)
    love.graphics.draw(self.image, (self.x * game.tile_w), ((self.y - game.map_y) * game.tile_h))
  end
  -- Cannonball collision filter
  function self.cannonball.collisionFilter(item, other)
    if other.isTower then return "cross"
    else return nil end
  end
  -- Function to play a sound
  function self.play_sound(sound)
     pitch = 0.8 + math.random(0, 10) / 25
     sound:setPitch(pitch)
     sound:play()
  end
end

-- Update the "game" gamestate
function game:update(dt)
  -- Update Sysl-Pixel
  gscreen.update(dt)
  -- Entities
  self.entities:update(dt)
  -- Enemies
  self.enemies:update(dt)
  -- Towers
  self.towers:update(dt)
  -- Update the UI
  self.uielements:update(dt)
  -- Update time
  self.time_playing = self.time_playing + ( dt / 30 )
  self.counter = self.counter + dt
  -- Check for movement
  local directions = {"up", "down", "left", "right"}
  for i, v in ipairs(directions) do
  if self.keyDown(v) then
    self.movement_que = v
  end
  end
  if self.counter > 0.03 then
    if self.movement_que == "down" then
      self.map_y = self.map_y + 1
      if self.map_y > self.map_h - self.map_display_h then
        self.map_y = self.map_h - self.map_display_h
      end
    end
    if self.movement_que == "up" then
      self.map_y = self.map_y - 1
      if self.map_y < 0 then
        self.map_y = 0
      end
    end
    if self.movement_que == "left" then
      self.map_x = self.map_x - 1
      if self.map_x < 0 then
        self.map_x = 0
      end
    end
    if self.movement_que == "right" then
      self.map_x = self.map_x + 1
      if self.map_x > self.map_w - self.map_display_w then
        self.map_x = self.map_w - self.map_display_w
      end
    end
    self.counter = 0
    self.movement_que = nil
    self.last_wave = self.wave
    self.wave = math.ceil(self.time_playing)
    if self.last_wave ~= self.wave then
      self.new_wave = true
    else
      self.new_wave = false
    end
  end
  -- Create new enemies based on a probability function
  local probability = self:enemyProbability()
  if probability > 0 then
    local createEnemy = true
    local rand = math.random(2, 17)
    for i, e in ipairs(self.enemies.enemyList) do
      if math.abs(e.x - rand) < 3 and math.abs(self.map_h - e.y) then
        createEnemy = false
      end
    end
    if createEnemy then
      self.enemies:add(self.enemy(self.entity, rand, 48, probability))
    end
  end
  -- Open the tower shop
  if love.mouse.isDown(1) and self.placing then
    if not game.hasShopmenu then
      self.uielements:add(self.shop(math.floor(gscreen.mouse.x / self.tile_w), math.floor((gscreen.mouse.y - (self.map_y * self.tile_h)) / self.tile_h)))
    end
  end
end

-- Draw the "game" gamestate
function game:draw()
  -- Scale stuff with Sysl-Pixel
  gscreen.start()
  -- Draw stuff
  -- Background
  love.graphics.setColor(44/255, 232/255, 244/255)
  love.graphics.rectangle("fill", 0, 0, 180, 320)
  love.graphics.setColor(1, 1, 1)
  -- Tilemap
  self:draw_map()
  -- Towers
  self.towers:draw()
  -- Enemies
  self.enemies:draw()
  -- Entities
  self.entities:draw()
  -- UI
  self.uielements:draw()
  -- Draw selection icon
  if self.placing then
     love.graphics.draw(self.placing_icon, math.floor(gscreen.mouse.x / self.tile_w) * self.tile_w,
                        math.floor(gscreen.mouse.y / self.tile_h) * self.tile_h)
  end
  -- Stop scaling stuff
  gscreen.stop()
end


-- Start the game
function love.load()
  gamestate.registerEvents()
  gamestate.switch(menu, 0, 0)
end


-- Function to draw the tilemap
function game:draw_map()
  for y=1, self.map_display_h do
    for x=1, self.map_display_w do                             
      love.graphics.draw( 
       self.tile[self.map[y+self.map_y][x+self.map_x]], 
       ((x-1)*self.tile_w)+self.map_offset_x, 
       ((y-1)*self.tile_h)+self.map_offset_y )
    end
  end
end
function gameover:draw_map()
  for y=1, self.map_display_h do
    for x=1, self.map_display_w do                             
      love.graphics.draw( 
       self.tile[self.map[y+self.map_y][x+self.map_x]], 
       ((x-1)*self.tile_w)+self.map_offset_x, 
       ((y-1)*self.tile_h)+self.map_offset_y )
    end
  end
end

-- Function to detect if keys are pressed
function game.keyDown(key)
  if key == "down" and (love.keyboard.isDown("down") or love.keyboard.isDown("s")) then
    return true
  elseif key == "up" and (love.keyboard.isDown("up") or love.keyboard.isDown("w")) then
    return true
  elseif key == "left" and (love.keyboard.isDown("left") or love.keyboard.isDown("a")) then
    return true
  elseif key == "right" and (love.keyboard.isDown("right") or love.keyboard.isDown("d")) then
    return true
  end
end

-- Probability function
function game:enemyProbability()
  local probability = math.sin(2*math.pi*self.time_playing) * math.sqrt(self.time_playing) * 2
  local rand = math.random(1, 100)
  if rand < probability / 2 then
    return(2)
  elseif rand < probability then
    return(1)
  end
  return(0)
end
function game.mousemoved( x, y, dx, dy, istouch )
  if (gscreen.mouse.y / game.tile_h) < 8 * game.tile_h then
     if game.map[math.floor(gscreen.mouse.y / game.tile_h) + 1][math.floor(gscreen.mouse.x / game.tile_w) + 1] <= 5 then
       game.placing = true
       gscreen.current_cursor = 3
     else
       game.placing = false
       if gscreen.mouse.y > 252 and game.hasShopmenu then
         gscreen.current_cursor = 1
       else
         gscreen.current_cursor = 2
       end
     end
  end
end
