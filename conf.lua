-- Configuration
function love.conf(t)
  t.title = "Invasion of the Sky Pirate Frogs"
  t.version = "11.3"
  t.window.width = 180
  t.window.height = 320
  t.window.resizable = false
  t.window.minwidth = 160
  t.window.minheight = 90
end
