# Invasion of the Sky Pirate Frogs

All code is licensed under the MIT license, however the game as a work of art and all the included assets are (unless otherwise noted) licensed under the CC BY-SA 4.0 license.

Copyright (c) 2022 Elijah Gregg
